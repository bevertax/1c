#include <iostream>
#include <map>
#include <memory>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <string_view>
#include <algorithm>

using std::string, std::string_view, std::shared_ptr;

struct Node {
public:
    shared_ptr<Node> operator [] (char letter) {
        if (!GetTransitionNode(letter)) {
            transitions_.insert({ letter, std::make_shared<Node>() });
            transitions_[letter]->parent_ = this;
            transitions_[letter]->transition_letter_ = letter;
            frequents_[letter] = 0;
        }
        if (letter == '$') {
            ++frequents_[letter];
            auto current_ptr = parent_;
            char transition_letter = transition_letter_;
            while (current_ptr != nullptr) {
                current_ptr->frequents_[transition_letter] = std::max(frequents_[letter], current_ptr->frequents_[transition_letter]);
                transition_letter = current_ptr->transition_letter_;
                current_ptr = current_ptr->parent_;
            }
        }

        return transitions_.at(letter);
    }

    shared_ptr<Node> GetTransitionNode(char letter) {
        auto transition = transitions_.find(letter);
        if (transition == transitions_.end()) {
            return nullptr;
        }

        return transition->second;
    }

    bool IsLeaf() {
        return transitions_.empty();
    }

    auto GetMostFrequentTransition() {
        size_t max_frequent = 0;
        char result_letter = 0;
        for (auto [letter, frequent]: frequents_) {
            if (frequent >= max_frequent) {
                max_frequent = frequent;
                result_letter = letter;
            }
        }
        return transitions_.find(result_letter);
    };

    char GetMostFrequentLetter() {
        return transitions_.begin()->first;
    }

private:
    std::map<char, shared_ptr<Node>> transitions_;
    Node* parent_ = nullptr;
    char transition_letter_ = 0;
    std::map<char, size_t> frequents_;
};

class T9 {
public:
    T9(): root_(std::make_shared<Node>()) {}

    void AddWords(const string& text) {
        std::stringstream words(text);
        std::string word;
        while (words >> word) {
            Push(word);
        }
    }

    string AutoComplete(string_view incomplete_word) {
        current_node_ = root_;
        failed_ = false;
        string result = Complete(incomplete_word);
        incomplete_word_ = incomplete_word;
        return result;
    }

    string Append(string_view addition) {
        string result = incomplete_word_ + Complete(addition);
        incomplete_word_ += addition;
        return result;
    }

private:
    string Complete(string_view incomplete_word) {
        string word(incomplete_word);

        for (char letter: word) {
            auto transition = current_node_->GetTransitionNode(letter);
            if (!transition || failed_) {
                failed_ = true;
                return word;
            }

            current_node_ = transition;
        }

        auto current_node = current_node_;

        while (!current_node->IsLeaf()) {
            auto transition = current_node->GetMostFrequentTransition();
            if (transition->first == '$') {
                break;
            }
            current_node = transition->second;
            word += transition->first;
        }

        return word;
    }

    void Push(string_view word) {
        auto current_node = root_;
        for (char letter: word) {
            current_node = (*current_node)[letter];
        }
        current_node = (*current_node)['$'];
    }

    shared_ptr<Node> root_ = std::make_shared<Node>();
    shared_ptr<Node> current_node_;
    string incomplete_word_;
    bool failed_ = false;
};

int main() {
    T9 t_9;
    string text = "abc abd abe abe ace ace ace acek acek";

    t_9.AddWords(text);
    std::cout << t_9.AutoComplete("a") << std::endl;
    std::cout << t_9.Append("b") << std::endl;
    std::cout << t_9.Append("let") << std::endl;
    std::cout << t_9.AutoComplete("b") << std::endl;
    std::cout << t_9.Append("b") << std::endl;
    return 0;
}